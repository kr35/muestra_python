#!/usr/bin/python3
# -*- coding: utf-8 -*-

from pymongo.errors import BulkWriteError
from model.model import MongoDB


class MongoLocal(MongoDB):

    def __init__(self):
        super().__init__('mongo_local')

    def get_active_searches(self):
        return [x for x in self._db['searches'].find({'active':1})]

    def many_update(self, data, collection):

        bulk = self._db[collection].initialize_unordered_bulk_op()
        counter = 0
        for item in data:
            bulk.find({'title': item['title']}).update({'$set': item})
            counter += 1

            if (counter % 500 == 0):
                bulk.execute()
                bulk = self._db[collection].initialize_ordered_bulk_op()

        if (counter % 500 != 0):
            bulk.execute()

    def insert_new_data(self, data, collection):
        if len(data) > 0:
            init_count = self._db['raw'].count({'id': {'$in':[x['id'] for x in data]}})

            try:
                self._db['raw'].insert_many(
                    data, bypass_document_validation=True,
                    ordered=False)
            except BulkWriteError as e:
                pass
            return len(data) - init_count
        else:
            return 0