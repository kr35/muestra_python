#!/usr/bin/python3
# -*- coding: utf-8 -*-

import settings.settings as settings
from pymongo import MongoClient


class MongoDB():

    def __init__(self, BD : str):
        config = {
          'user': settings.settings[BD]['user'],
          'password': settings.settings[BD]['pass'],
          'host': settings.settings[BD]['host'],
          'database': settings.settings[BD]['dbname'],
          'port': int(settings.settings[BD]['port'])
        }
        self._conn = MongoClient(
            ':'.join((config['host'], str(config['port']))))

        self._db = self._conn[config['database']]

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self._conn.close()

    def __del__(self):
        pass
