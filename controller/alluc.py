#!/usr/bin/python3
# -*- coding: utf-8 -*-

from core.utils import generate_hash
from model.mongo_local import MongoLocal

import json
import requests
import datetime
import time


class Alluc():

    def __init__(self):

        self.start()

    def get_data(self):

        print("getting data")
        with MongoLocal() as BD:
            titles = BD.get_active_searches()

        return titles

    def start(self):

        while True:

            searches = self.get_data()

            for search in searches:

                print('*'*20)
                print("{0} started scrape".format(search['title']))

                string_time = self.analyze_time(search.get('date_last_search', None))

                links = self.get_all_links(search['title'],search['site'], string_time, search['limit'], search['key'])

                print("{0} links finally obtained".format(str(len(links))))

                with MongoLocal() as BD:
                    n_inserted = BD.insert_new_data(links, 'raw')

                print("{0} links inserted".format(str(n_inserted)))

                search['date_last_search'] = str(datetime.datetime.today().strftime("%Y-%m-%d %H:%M:%S"))


            with MongoLocal() as BD:
                BD.many_update(searches, 'searches')

            print('*' * 20)
            print("finish")
            print("next process {0}".format(
                datetime.datetime.fromtimestamp(
                    time.time() + 43200).strftime('%Y-%m-%d %H:%M:%S')))

            time.sleep(43200)

    def analyze_time(self, time):

        if time:
            time = datetime.datetime.strptime(str(time), "%Y-%m-%d %H:%M:%S")

            actual_time = datetime.datetime.today()

            days_passed = (actual_time - time).days

            if days_passed == 0:
                return '+%23last24h'
            elif days_passed < 7:
                return '+%23lastweek'
            elif days_passed < 30:
                return '+%23lastmonth'
            else:
                return ''
        else:
            return ''

    def request_with_proxie(self, url: str):

        try:
            response = requests.get(
                url,
                timeout=25)

            return {
                'status_code': response.status_code,
                'text': response.text
            }

        except:
            return {
                'status_code': -1,
                'text': ''
            }

    def get_all_links(self, title, site, time, limit, key):

        links = []
        result_count = 0

        if limit > 100:
            count = 100
        else:
            count = limit

        init = 0

        while init > -1:
            url =  "http://www.alluc.ee/api/search/{0}/?apikey={1}&query={2}{3}&count={4}&from={5}&getmeta=0".format(
                site, key, title.replace(' ','+'), time, count, init
            )

            response = self.request_with_proxie(url)

            if response['status_code'] == 200:
                json_response = json.loads(response['text'])
                if json_response['status'] == "success":
                    for link in json_response['result']:
                        for hostedurl in link['hosterurls']:
                            links.append({
                                'id': generate_hash(hostedurl['url']),
                                'url': hostedurl['url'],
                                'id_alluc': hostedurl['filedataid'],
                                'title': link['title'],
                                'parent_title': link['sourcetitle'],
                                'lang': link['lang'],
                                'hoster': link['hostername'],
                                'parent_url': link['sourceurl'],
                                'created_date': link['created'],
                                'search': title,
                                'site': site,
                                'tags': [x.strip() for x in link['tags'].split(",")]
                            })
                result_count += len(json_response['result'])
                if result_count < json_response['resultcount'] and result_count < limit:
                    init += 100
                    if limit - init < 100:
                        count = limit - init
                    else:
                        count = 100
                else:
                    init = -1
            else:
                init = -1

        return links
