#!/usr/bin/python3
# -*- coding: utf-8 -*-

import hashlib

def generate_hash(url):
    aux = hashlib.sha224()
    aux.update(url.encode('utf-8'))
    return aux.hexdigest()
