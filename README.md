# Introduccion #

Este proyecto se trata de un scraper realizado para alluc (http://www.alluc.ee/) mediante el uso de su api.
Todos los datos necesarios para buscar en la pagina como el titulo de busqueda o la key se guardan en la base de datos MongoDB (https://www.mongodb.com/) donde tambien se guardan los resultados

**WARNING:** La api de alluc esta limitada en el caso de las cuentas gratuitas a 200 links al dia, para aumentarlo es necesario logearte en su pagina y resolver un captcha

# Instalacion
Para que el scraper funciona es necesario tener instalado el driver para MongoDB de python pymongo y el paquete requests para realizar las peticiones.
Para obtener el proyecto e instalar las dependencias hay que ejecutar estos comandos en la terminal

    $ git clone https://kr35@bitbucket.org/kr35/muestra_python.git
    $ cd muestra_python/
    $ pip install -r requirements.txt  #add '--user' if you are not root

# Uso #

Para ejecutarlo solo es necesario ir a la carpeta del proyecto y ejecutar esta instruccion

    $ python3 ./main.py
