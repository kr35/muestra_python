#!/usr/bin/python3
# -*- coding: utf-8 -*-

from configparser import SafeConfigParser
from os import path

settings = dict()
APP_PATH = path.dirname(path.dirname(path.abspath(__file__)))
ROOT_PATH = path.dirname(APP_PATH)


def basepath(*path_add):
    return path.join(APP_PATH, *path_add)


# **************************************************************
# DIRECTORY
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Sistema de archivos, rutas a directorios de la aplicación
#
# storage: Directorio principal de almacenamiento
# tmp: Directorio temporal
# install: Directorio con contenido de instalación
# dump: Ficheros dump en caso de errores criticos
# uploads: Directorio subida de ficheros
# vault: Directorio de certificados
# template: Directorio base de templates
# schema: Directorio de definición de schemas
# ------------------------------------------------------------//
settings['storage.path'] = basepath('storage')
settings['tmp.path'] = basepath('storage', 'tmp')
settings['install.path'] = basepath('storage', 'install')
settings['dump.path'] = basepath('storage', 'dump')
settings['uploads.path'] = basepath('storage', 'uploads')


# **************************************************************
# CONFIGURATION
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Archivos de configuración
#
# main: Fichero de configuración principal
# env.main: Fichero de configuración principal del entorno
# ------------------------------------------------------------//
settings['config.file.main'] = basepath('config', 'main.ini')
# settings['config.file.env'] = basepath('config', 'main.env.ini')
# Gestor de configuración
config = SafeConfigParser(allow_no_value=True)
config.read((settings['config.file.main'],))
# Mongo Local
settings['mongo_local'] = {}
settings['mongo_local']['host'] = config.get('MONGODB_LOCAL', 'host')
settings['mongo_local']['port'] = config.get('MONGODB_LOCAL', 'port')
settings['mongo_local']['user'] = config.get('MONGODB_LOCAL', 'user')
settings['mongo_local']['pass'] = config.get('MONGODB_LOCAL', 'passw')
settings['mongo_local']['dbname'] = config.get('MONGODB_LOCAL', 'dbname')